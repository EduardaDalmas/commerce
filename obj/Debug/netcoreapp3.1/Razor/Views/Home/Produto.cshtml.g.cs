#pragma checksum "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9a44980fa3c28c96a44c06e6637bdf313b1e605b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Produto), @"mvc.1.0.view", @"/Views/Home/Produto.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/_ViewImports.cshtml"
using Commerce;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/_ViewImports.cshtml"
using Commerce.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9a44980fa3c28c96a44c06e6637bdf313b1e605b", @"/Views/Home/Produto.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"722218f3dee4d05c12a7b769fa3929121879b3cd", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Produto : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Produto>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml"
  
    ViewData["Title"] = Model.Nome;

#line default
#line hidden
#nullable disable
            WriteLiteral("\n\n<div>\n    <h1>");
#nullable restore
#line 8 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml"
   Write(Model.Nome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\n    <p>Descrição: ");
#nullable restore
#line 9 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml"
             Write(Model.Descricao);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n    <p>Valor: R$ ");
#nullable restore
#line 10 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml"
            Write(Model.Valor);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n    <p>Unidades Disponíveis: ");
#nullable restore
#line 11 "/Users/eduardabrischdalmas/Documents/IENH/Commerce/Views/Home/Produto.cshtml"
                        Write(Model.Estoque);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n</div>\n\n\n\n\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Produto> Html { get; private set; }
    }
}
#pragma warning restore 1591
